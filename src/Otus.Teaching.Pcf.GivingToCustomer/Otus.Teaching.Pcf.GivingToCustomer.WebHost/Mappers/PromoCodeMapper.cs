﻿using System;
using System.Collections.Generic;
using Otus.Teaching.Pcf.GivingToCustomer.Core.Domain;
using Otus.Teaching.Pcf.GivingToCustomer.WebHost.Models;

 namespace Otus.Teaching.Pcf.GivingToCustomer.WebHost.Mappers
{
    public class PromoCodeMapper
    {
        public static PromoCode MapFromModel(GivePromoCodeRequest request, Preference preference, IEnumerable<Customer> customers) {

            var promocode = new PromoCode
            {
                Id = request.PromoCodeId,
                PartnerId = request.PartnerId,
                Code = request.PromoCode,
                ServiceInfo = request.ServiceInfo,
                BeginDate = DateTime.TryParse(request.BeginDate, out var parsedBegin) 
                    ? parsedBegin
                    : DateTime.Now,
                EndDate = DateTime.TryParse(request.BeginDate, out var parsedEnd) 
                    ? parsedEnd
                    : DateTime.Now.AddDays(1),
                Preference = preference,
                PreferenceId = preference.Id,
                Customers = new List<Customer>()
            };

            foreach (var item in customers)
            {
                promocode.Customers.Add(new Customer()
                {
                    Id = item.Id,
                    FirstName = item.FirstName,
                    LastName = item.LastName,
                    Email = item.Email
                });
            };
            
            return promocode;
        }
    }
}
