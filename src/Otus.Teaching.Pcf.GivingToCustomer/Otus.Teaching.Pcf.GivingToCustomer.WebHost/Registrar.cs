﻿namespace Otus.Teaching.Pcf.GivingToCustomer.WebHost
{
    using Microsoft.Extensions.Configuration;
    using Microsoft.Extensions.DependencyInjection;
    using MongoDB.Bson.Serialization;
    using MongoDB.Driver;
    using Otus.Teaching.Pcf.GivingToCustomer.Core.Domain;
    using Otus.Teaching.Pcf.GivingToCustomer.DataAccess.Mongo;

    public static class Registrar
    {
        public static IServiceCollection AddMongoDbContext(this IServiceCollection services, IConfiguration configuration)
        {
            IMongoClient client = new MongoClient(configuration.GetConnectionString("MongoConnection"));
            //IMongoClient client = new MongoClient("mongodb://admin:test@localhost:65066");

            RegisterMappings();

            var mongoDataContext = new MongoDataContext(client, configuration["MongoSettings:DatabaseName"]);

            return services.AddSingleton(mongoDataContext);
        }

        private static void RegisterMappings()
        {
            BsonClassMap.RegisterClassMap<Customer>(classMap =>
            {
                classMap.AutoMap();
                classMap.SetIgnoreExtraElements(true);
            });
        }
    }
}
