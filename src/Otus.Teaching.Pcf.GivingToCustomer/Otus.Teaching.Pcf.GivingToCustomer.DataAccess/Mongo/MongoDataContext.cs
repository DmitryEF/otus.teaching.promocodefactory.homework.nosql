﻿namespace Otus.Teaching.Pcf.GivingToCustomer.DataAccess.Mongo
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using MongoDB.Driver;
    using Otus.Teaching.Pcf.GivingToCustomer.Core.Domain;


    public class MongoDataContext : BaseMongoDbContext
    {
        public MongoDataContext(IMongoClient client, string databaseName) 
            : base(client.GetDatabase(databaseName))
        {
        }

        public IEnumerable<Preference> Preferences => this.GetAll<Preference>("Preferences").ToList();

        public IEnumerable<Customer> Customers => this.GetAll<Customer>("Customers").ToList();

        public IEnumerable<PromoCode> PromoCodes => this.GetAll<PromoCode>("PromoCodes").ToList();

        public IEnumerable<T> Set<T>() where T : BaseEntity
        {
            return typeof(T).Name switch
            {
                "Preference" => this.Preferences as IEnumerable<T>,
                "Customer" => this.Customers as IEnumerable<T>,
                "PromoCode" => this.PromoCodes as IEnumerable<T>,
                _ => throw new ArgumentException("Invalid type of BaseEntity")
            };
        }
    }
}
