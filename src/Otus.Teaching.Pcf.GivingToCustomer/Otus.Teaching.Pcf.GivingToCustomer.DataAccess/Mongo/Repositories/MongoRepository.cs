﻿namespace Otus.Teaching.Pcf.GivingToCustomer.DataAccess.Mongo.Repositories
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Linq.Expressions;
    using System.Threading.Tasks;
    using Otus.Teaching.Pcf.GivingToCustomer.Core.Abstractions.Repositories;
    using Otus.Teaching.Pcf.GivingToCustomer.Core.Domain;

    public class MongoRepository<T> : IRepository<T> where T : BaseEntity
    {
        private readonly MongoDataContext _dataContext;

        private string _collectionName;

        public MongoRepository(MongoDataContext dataContext)
        {
            this._dataContext = dataContext;
        }

        public void InitializeCollectionName(string collectionName)
        {
            this._collectionName = collectionName;
        }

        public Task<IEnumerable<T>> GetAllAsync()
        {
            return Task.FromResult(this._dataContext.Set<T>());
        }

        public Task<T> GetByIdAsync(Guid id)
        {
            return Task.FromResult(
                this._dataContext.Set<T>()
                    .FirstOrDefault(t => t.Id == id));
        }

        public Task<IEnumerable<T>> GetRangeByIdsAsync(List<Guid> ids)
        {
            if (ids == null) return null;

            return Task.FromResult(
                this._dataContext.Set<T>()
                    .Where(e => ids.Contains(e.Id)));
        }

        public Task<T> GetFirstWhere(Expression<Func<T, bool>> predicate)
        {
            throw new NotImplementedException();
        }

        public Task<IEnumerable<T>> GetWhere(Expression<Func<T, bool>> predicate)
        {
            return this._dataContext.GetWhere(predicate, this._collectionName);
        }

        public async Task AddAsync(T entity)
        {
            await this._dataContext.AddRangeAsync(
                new List<T>{entity}, this._collectionName);
        }

        public Task UpdateAsync(T entity)
        {
            return this._dataContext.UpdateAsync(entity, this._collectionName);
        }

        public Task DeleteAsync(T entity)
        {
            return this._dataContext.DeleteAsync(entity, this._collectionName);
        }
    }
}
