﻿namespace Otus.Teaching.Pcf.GivingToCustomer.DataAccess.Mongo.Data
{
    using System.Linq;
    using Otus.Teaching.Pcf.GivingToCustomer.DataAccess.Data;

    public class MongoDbInitializer : IDbInitializer
    {
        private readonly MongoDataContext mongoDataContext;

        public MongoDbInitializer(MongoDataContext mongoDataContext)
        {
            this.mongoDataContext = mongoDataContext;
        }

        public void InitializeDb()
        {
            if(this.mongoDataContext.Preferences == null || !this.mongoDataContext.Preferences.Any())
                this.mongoDataContext.AddRangeAsync(FakeDataFactory.Preferences, "Preferences").Wait();

            if (this.mongoDataContext.Customers == null || !this.mongoDataContext.Customers.Any())
                this.mongoDataContext.AddRangeAsync(FakeDataFactory.Customers, "Customers").Wait();
        }
    }
}
