﻿namespace Otus.Teaching.Pcf.GivingToCustomer.DataAccess.Mongo;

using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Reflection;
using System.Threading.Tasks;
using MongoDB.Bson;
using MongoDB.Driver;
using Otus.Teaching.Pcf.GivingToCustomer.Core.Domain;

public class BaseMongoDbContext
{
    protected readonly IMongoDatabase _database;

    public BaseMongoDbContext(IMongoDatabase database)
    {
        this._database = database;
    }

    public async Task AddRangeAsync<T>(IEnumerable<T> entities, string collectionName) 
        where T : BaseEntity
    {
        var collection = this._database.GetCollection<T>(collectionName);

        await collection.InsertManyAsync(entities);
    }

    protected IEnumerable<T> GetAll<T>(string collectionName)
    {
        var collection = this._database.GetCollection<T>(collectionName);

        return collection.Find(new BsonDocument()).ToList();
    }

    public async Task UpdateAsync<T>(T entity, string collectionName)
        where T : BaseEntity
    {
        if (string.IsNullOrEmpty(collectionName))
            throw new ArgumentException("Wrong type of collection");

        var filter = Builders<T>.Filter.Eq("Id", entity.Id);

        var builder = Builders<T>.Update;

        var updateLoggingDefinitions = (
            from p in typeof(T).GetProperties().Where(p => p.Name != "Id")
            where p.GetValue(entity) != null
            let collection = p.GetValue(entity)
            where collection is not System.Collections.ICollection { Count: 0 }
            select builder.Set(p.Name, p.GetValue(entity))).ToList();

        await this._database.GetCollection<T>(collectionName)
            .UpdateOneAsync(filter, builder.Combine(updateLoggingDefinitions));
    }

    public async Task DeleteAsync<T>(T entity, string collectionName)
        where T : BaseEntity
    {
        if (string.IsNullOrEmpty(collectionName))
            throw new ArgumentException("Wrong type of collection");

        var filter = Builders<T>.Filter.Eq("Id", entity.Id);

        await this._database.GetCollection<T>(collectionName).DeleteOneAsync(filter);
    }

    public async Task<IEnumerable<T>> GetWhere<T>
        (Expression<Func<T, bool>> predicate, string collectionName) where T : BaseEntity
    {
        return
            (await this._database.GetCollection<T>(collectionName)
                .FindAsync(predicate)).ToList();
    }
}