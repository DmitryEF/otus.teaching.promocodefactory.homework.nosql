﻿namespace Otus.Teaching.Pcf.Administration.WebHost
{
    using Microsoft.Extensions.Configuration;
    using Microsoft.Extensions.DependencyInjection;
    using MongoDB.Bson.Serialization;
    using MongoDB.Driver;
    using Otus.Teaching.Pcf.Administration.Core.Domain.Administration;
    using Otus.Teaching.Pcf.Administration.DataAccess.Mongo;

    public static class Registrar
    {
        public static IServiceCollection AddMongoDbContext(this IServiceCollection services, IConfiguration configuration)
        {
            IMongoClient client = new MongoClient(configuration.GetConnectionString("MongoConnection"));
 
            RegisterMappings();

            var mongoDataContext = new MongoDataContext(client, configuration["MongoSettings:DatabaseName"]);

            return services.AddSingleton(mongoDataContext);
        }

        private static void RegisterMappings()
        {
            BsonClassMap.RegisterClassMap<Employee>(classMap =>
            {
                classMap.AutoMap();
                classMap.SetIgnoreExtraElements(true);
            });
        }
    }
}
