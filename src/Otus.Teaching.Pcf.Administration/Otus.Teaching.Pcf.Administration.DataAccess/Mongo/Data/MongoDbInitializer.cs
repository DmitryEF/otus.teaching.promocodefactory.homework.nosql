﻿namespace Otus.Teaching.Pcf.Administration.DataAccess.Mongo.Data
{
    using System.Linq;
    using Otus.Teaching.Pcf.Administration.DataAccess.Data;

    public class MongoDbInitializer : IDbInitializer
    {
        private readonly MongoDataContext mongoDataContext;

        public MongoDbInitializer(MongoDataContext mongoDataContext)
        {
            this.mongoDataContext = mongoDataContext;
        }

        public void InitializeDb()
        {
            if(this.mongoDataContext.Roles == null || !this.mongoDataContext.Roles.Any())
                this.mongoDataContext.AddRangeAsync(FakeDataFactory.Roles, "Roles").Wait();

            if (this.mongoDataContext.Employees == null || !this.mongoDataContext.Employees.Any())
                this.mongoDataContext.AddRangeAsync(FakeDataFactory.Employees, "Employees").Wait();
        }
    }
}
