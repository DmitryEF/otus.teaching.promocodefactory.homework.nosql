﻿namespace Otus.Teaching.Pcf.Administration.DataAccess.Mongo;

using System.Collections.Generic;
using System.Threading.Tasks;
using MongoDB.Bson;
using MongoDB.Driver;
using Otus.Teaching.Pcf.Administration.Core.Domain;

public class BaseMongoDbContext
{
    protected readonly IMongoDatabase _database;

    public BaseMongoDbContext(IMongoDatabase database)
    {
        this._database = database;
    }

    public async Task AddRangeAsync<T>(IEnumerable<T> entities, string collectionName) 
        where T : BaseEntity
    {
        var collection = this._database.GetCollection<T>(collectionName);

        await collection.InsertManyAsync(entities);
    }

    protected IEnumerable<T> GetAll<T>(string collectionName)
    {
        var collection = this._database.GetCollection<T>(collectionName);

        return collection.Find(new BsonDocument()).ToList();
    }
}