﻿namespace Otus.Teaching.Pcf.Administration.DataAccess.Mongo
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;
    using MongoDB.Driver;
    using Otus.Teaching.Pcf.Administration.Core.Domain;
    using Otus.Teaching.Pcf.Administration.Core.Domain.Administration;

    public class MongoDataContext : BaseMongoDbContext
    {
        public MongoDataContext(IMongoClient client, string databaseName) 
            : base(client.GetDatabase(databaseName))
        {
        }

        public IEnumerable<Role> Roles => this.GetAll<Role>("Roles").ToList();

        public IEnumerable<Employee> Employees => this.GetAll<Employee>("Employees").ToList();

        public IEnumerable<T> Set<T>() where T : BaseEntity
        {
            return typeof(T).Name switch
            {
                "Role" => this.Roles as IEnumerable<T>,
                "Employee" => this.Employees as IEnumerable<T>,
                _ => throw new ArgumentException("Invalid type of BaseEntity")
            };
        }

        public async Task UpdateAsync<T>(T entity)
            where T : BaseEntity
        {
            var collectionName = entity.GetType().Name switch
            {
                "Role" => "Roles",
                "Employee" => "Employees",
                _ => string.Empty
            };

            if (string.IsNullOrEmpty(collectionName))
                throw new ArgumentException("Wrong type of entity");

            var filter = Builders<T>.Filter.Eq("Id", entity.Id);

            var builder = Builders<T>.Update;

            var updateLoggingDefinitions = (
                from p in typeof(T).GetProperties().Where(p => p.Name != "Id")
                where p.GetValue(entity) != null
                let collection = p.GetValue(entity)
                where collection is not System.Collections.ICollection { Count: 0 }
                select builder.Set(p.Name, p.GetValue(entity))).ToList();

            await this._database.GetCollection<T>(collectionName)
                .UpdateOneAsync(filter, builder.Combine(updateLoggingDefinitions));
        }
    }
}
