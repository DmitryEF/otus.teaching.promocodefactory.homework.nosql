﻿namespace Otus.Teaching.Pcf.Administration.DataAccess.Mongo.Repositories
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Linq.Expressions;
    using System.Threading.Tasks;
    using Otus.Teaching.Pcf.Administration.Core.Abstractions.Repositories;
    using Otus.Teaching.Pcf.Administration.Core.Domain;

    public class MongoRepository<T> : IRepository<T> where T : BaseEntity
    {
        private readonly MongoDataContext _dataContext;

        public MongoRepository(MongoDataContext dataContext)
        {
            this._dataContext = dataContext;
        }

        public Task<IEnumerable<T>> GetAllAsync()
        {
            return Task.FromResult(this._dataContext.Set<T>());
        }

        public Task<T> GetByIdAsync(Guid id)
        {
            return Task.FromResult(
                this._dataContext.Set<T>()
                    .FirstOrDefault(t => t.Id == id));
        }

        public Task<IEnumerable<T>> GetRangeByIdsAsync(List<Guid> ids)
        {
            return Task.FromResult(
                this._dataContext.Set<T>()
                    .Where(e => ids.Contains(e.Id)));
        }

        public Task<T> GetFirstWhere(Expression<Func<T, bool>> predicate)
        {
            throw new NotImplementedException();
        }

        public Task<IEnumerable<T>> GetWhere(Expression<Func<T, bool>> predicate)
        {
            throw new NotImplementedException();
        }

        public Task AddAsync(T entity)
        {
            throw new NotImplementedException();
        }

        public Task UpdateAsync(T entity)
        {
            return this._dataContext.UpdateAsync(entity);
        }

        public Task DeleteAsync(T entity)
        {
            throw new NotImplementedException();
        }
    }
}
